What is the webcast program at GitLab?
--------------------------------------

-   The webcast program consists of regular online events for GitLab users and fans.
-   The webcasts are recorded and distributed to GitLab users, and can be referred to in the Resource Library at About.GitLab.com


Monthly Schedule
----------------


### GitLab 101 - 20 mins

-   Live demo run; with sales/account team member
-   Aimed at developers > drawing them into the community.
-   Same base demo script each month. 
    -   How to use GitLab - essential demo.
    -   This month’s Q+A (commonly asked question, recent question from twitter) 
    -   GitLab’s products and services
    -   Community welcome mat: Where to find help in the community, how to meet other GitLab users what events we’ll be at, how about you?


### Release Party - 30 mins

-   Monthly on the 23rd or the next nearest business day.
-   Present highlights from the new features. 
-   Refer to any resources, docs, screencasts, etc. 
-   Guest speakers from the dev team about the new features. 
-   Highlight contributors. External contributors welcome.
-   Q+A from audience. 


### GitLab - Deep dive (better name?) - 1 hour

-   Monthly in-depth learning theme. 
-   Preceded by 3 weeks of blog posts, screencasts, tutorial and invitation which leads to the online event. 
-   Live event includes: Guest speaker / interview / presentation / demo as appropriate to topic.
-   Q+A from audience. Survey to GitLab users on the topic if appropriate. 
-   After event: Blog post of findings from the Q+A, results of survey.
-   Roll-up content into an downloadable ebook.