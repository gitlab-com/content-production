Monthly themes
==============

Each month, we’ll dig into a theme in-depth. In addition to micro content such
as screencasts, tutorials and blogs, we’ll dive into a big-picture topic each
month.

Taking perspectives
-------------------

Each month, we explore a theme from a number of perspectives. For example if we
are talking about continuous integration, we will dive into the practical and
technical steps for GitLab CI users; but we will also make it comprehensible to
non-technical team members and explore the wider implications for decision
makers

This will help us look at the big-picture and take a whole-organization approach to communicating the benefits of GitLab.


-   Level 100: Overview and brief summary of tool, technique or concept in the
    monthly theme.

    -   Start off with a basic introduction to the topic.
    -   Assumes little or no expertise with topic and covers topic concepts,
        functions, features, and benefits.
    -   Output - Brief blog post: Introduction to theme, point to existing
        resources. Open the conversation.

-   Level 200: Practical steps on using the tool, technique or concept.

    -   Provide specific details about the topic.
    -   Explore the practical and essential “how-to”; How can you be more
        effective?
    -   Output - Another blog post, could include tutorial content, new
        documentation.

-   Level 300: Technical / Under the hood / In more depth

    -   Understanding of features in a real-world environment. Detailed
        technical overview of a subset of product/technology features, covering
        architecture, performance, migration, deployment, and development.
    -   Or, if it’s not a technical theme -  Dig into the organizational
        implications for decisions about various approaches.
    -   Output - A blog post pointing to screencast or Detailed tutorial,
        documentation.

-   Level 400: Expert-to-expert

    -   Expert-to-expert interaction and coverage of specialized topics.

    -   Output

        -   A live online webcast with subject matter experts. Internal subject
            matter expert, guest speaker from the industry, GitLab clients or
            partners.
        -   Survey, quiz, online discussion.
        -   Recording of the webcast

 

Monthly themes
--------------


### December theme “Retrospective”

### January theme “Ship it! Release approaches and you” 

-   Week 1: Comparing approaches to release management

-   Week 2: How does GitLab manage releases?

-   Week 3: Release management with GitLab

-   Week 4: Webcast on release management

-   Post-webcast - blog post including recording, highlights and findings from
    the survey.

### February theme “Continuous Integration: How the game has changed”

-   Week 1: What is continuous integration? What makes GitLab CI different?

-   Week 2: GitLab CI - how to use it - screencast and tutorial

-   Week 3: Deep-dive, how does it work underneath. Blog by Kamil

-   Week 4: Webcast on Continous integration

-   Post-webcast - blog post including recording, highlights and findings from
    the survey.

### March theme “The Business of Open Source”

-   Week 1: What are some open source business models?

-   Week 2: What is GitLab’s business model? Sid on open core.

-   Week 3: Selling open source. Blog by Richard/Ron about selling open source.

-   Week 4: Webcast on The Business of Open Source -  Guest speaker or panel.

-   April theme “Open source community building”